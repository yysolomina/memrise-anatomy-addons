// ==UserScript==
// @name         Memrise new image styles for Anatomy course
// @namespace    https://gitlab.com/yysolomina/memrise-anatomy-addons
// @updateURL      https://gitlab.com/yysolomina/memrise-anatomy-addons/raw/master/newImageStyles.js
// @downloadURL    https://gitlab.com/yysolomina/memrise-anatomy-addons/raw/master/newImageStyles.js
// @version      0.1
// @description  New Image Styles for anatomy course in Memrise web big screens
// @author       Solomina Yuliya Y (yysolomina)
// @match        https://www.memrise.com/course/1553974/*
// @grant GM_addStyle
// ==/UserScript==

console.log('Start running custom yysolomina script...');
(function() {
    'use strict';

    GM_addStyle('.thing .col.image img { max-height: 15em; } ');
    GM_addStyle('.thing-show .row.primary .row-value { font-size: 2em; } ');
    GM_addStyle('.thing-show .row .row-value img { max-height: 17em; } ');
    GM_addStyle('.question-row .qquestion img { max-height: 17em; } ');

    console.log('Finish running custom yysolomina script');
})();